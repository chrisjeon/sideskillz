source 'https://rubygems.org'

gem 'aasm'
gem 'activeadmin', github: 'gregbell/active_admin'
gem 'active_model_serializers'
gem 'autoprefixer-rails'
gem 'binding_of_caller'
gem 'bootstrap-sass', '~> 3.2.0'
gem 'cancancan'
gem 'chartkick'
gem 'compass-rails'
gem 'devise'
gem 'faker'
gem 'font-awesome-rails'
gem 'friendly_id', '~> 5.0.0'
gem 'geokit'
gem 'geokit-rails'
gem 'groupdate'
gem 'haml-rails'
gem 'jbuilder', '~> 2.0'
gem 'jquery-rails'
gem 'paperclip'
gem 'pg'
gem 'sass-rails', '~> 4.0.3'
gem 'rails', '4.1.5'
gem 'rails_12factor'
gem 'sdoc', '~> 0.4.0',          group: :doc
gem 'searchkick'
gem 'uglifier', '>= 1.3.0'
# gem 'therubyracer',  platforms: :ruby

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

gem 'capistrano', '~> 3.1.0'
gem 'capistrano-rails', '~> 1.1.0'
gem 'capistrano-bundler'
gem 'capistrano-rbenv', '~> 2.0'

group :development do
  gem 'spring'
  gem 'awesome_print'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'better_errors'
  gem 'rspec-rails'
end

group :test do
  gem 'simplecov'
  gem 'launchy'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'shoulda-matchers', require: false
  gem 'vcr'
  gem 'webmock', '< 1.16'
  gem 'selenium-webdriver'
end
