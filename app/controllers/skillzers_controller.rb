class SkillzersController < ApplicationController
  respond_to :json, :html

  before_action :find_skillzer, only: :show

  def index
    @skillzers = Skillzer.all
    respond_with @skillzers
  end

  def show
    respond_with @skillzer
  end

  private

  def find_skillzer
    @skillzer = Skillzer.find(params[:id])
  end
end