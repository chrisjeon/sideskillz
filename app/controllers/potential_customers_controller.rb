class PotentialCustomersController < ApplicationController
  def index
  end

  def new
    @potential_customer = PotentialCustomer.new
  end

  def create
    @potential_customer = PotentialCustomer.new(potential_customer_params)
    if @potential_customer.save
      render 'confirmation'
    else
      render 'new'
    end
  end

  def show
    @potential_customer = PotentialCustomer.find(params[:id])
  end

  private

  def potential_customer_params
    params.require(:potential_customer).permit(:email, :type, :description)
  end
end
