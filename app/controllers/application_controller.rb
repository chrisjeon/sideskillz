class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :type
    devise_parameter_sanitizer.for(:sign_up) << :first_name
    devise_parameter_sanitizer.for(:sign_up) << :last_name
    devise_parameter_sanitizer.for(:sign_up) << :company_name
    devise_parameter_sanitizer.for(:sign_up) << :profile_picture
    devise_parameter_sanitizer.for(:account_update) << :type
    devise_parameter_sanitizer.for(:account_update) << :first_name
    devise_parameter_sanitizer.for(:account_update) << :last_name
    devise_parameter_sanitizer.for(:account_update) << :company_name
    devise_parameter_sanitizer.for(:account_update) << :profile_picture
  end

  def after_sign_in_path_for(resource)
    user_path(resource)
  end
end