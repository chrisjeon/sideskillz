class PotentialCustomer < ActiveRecord::Base
  self.inheritance_column = nil
  validates :email, 
    presence: {message: "Please enter your email address."}, 
    uniqueness: {message: "You already signed up."}
  validates :type, 
    presence: {message: "Please indicate why you would join."}
  validates :description, 
    presence: {message: "Please describe why you would join."}
end
