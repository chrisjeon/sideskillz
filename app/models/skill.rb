class Skill < ActiveRecord::Base
  has_many :user_skills
  has_many :users, through: :user_skills
  has_many :project_skills
  has_many :projects, through: :project_skills

  has_attached_file :picture

  validates_attachment :picture,
    content_type: { content_type: ["image/jpeg", "image/png"] }
end
