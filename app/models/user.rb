class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  extend FriendlyId
  friendly_id :fullname, use: :slugged

  has_many :user_skills, dependent: :destroy
  has_many :skills, through: :user_skills, dependent: :destroy
  has_many :reviews, as: :reviewable

  has_attached_file :profile_picture

  validates_attachment :profile_picture, :content_type => { :content_type => "image/jpeg" }

  def fullname
    "#{first_name} #{last_name}"
  end

  def skillzer?
    type == 'Skillzer'
  end

  def skill_seeker?
    type == 'SkillSeeker'
  end
end
