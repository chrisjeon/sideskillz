class UserSkill < ActiveRecord::Base
  has_many :reviews, as: :reviewable
  belongs_to :user
  belongs_to :skill
end
