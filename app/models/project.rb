class Project < ActiveRecord::Base
  extend FriendlyId
  include AASM

  friendly_id :name, use: :slugged

  aasm do
    state :unstarted, initial: true
    state :started

    event :start do
      transitions from: :unstarted, to: :started
    end
  end

  has_many :project_skills
  has_many :skills, through: :project_skills

  has_attached_file :picture

  validates_attachment :picture, :content_type => { :content_type => "image/jpeg" }
end
