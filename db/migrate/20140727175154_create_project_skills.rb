class CreateProjectSkills < ActiveRecord::Migration
  def change
    create_table :project_skills do |t|
      t.references :skill, index: true
      t.references :project, index: true

      t.timestamps
    end
  end
end
