class AddPictureToSkills < ActiveRecord::Migration
  def change
    add_attachment :skills, :picture
  end
end
