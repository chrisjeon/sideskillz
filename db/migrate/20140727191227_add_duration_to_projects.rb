class AddDurationToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :duration, :integer
  end
end
