class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.attachment :picture

      t.timestamps
    end
  end
end
