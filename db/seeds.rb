User.all.map(&:destroy)

nina = User.create(first_name: 'Nina',
                   last_name: 'Yang',
                   email: 'ninaqy13@gmail.com',
                   password: 'password',
                   password_confirmation: 'password',
                   profile_picture: File.new('app/assets/images/nina.jpg'))

Skill.all.map(&:destroy)
[{ name: 'Copywriting', image: File.new('app/assets/images/icon_project_copywriting.png') },
 { name: 'Guitar', image: File.new('app/assets/images/icon_profile_guitar.png') },
 { name: 'Singing', image: File.new('app/assets/images/icon_profile_singing.png') },
 { name: 'Photography', image: File.new('app/assets/images/icon_profile_photography.png') },
 { name: 'Writing', image: File.new('app/assets/images/icon_project_writing.png') },
 { name: 'Editing', image: File.new('app/assets/images/icon_project_editing.png') }].each do |skill_data|
  Skill.create(name: skill_data[:name], picture: skill_data[:image])
end

3.times do |i|
  nina.user_skills.create skill: Skill.find(i + 1),
                          rating: (1..5).to_a.sample
end

["Cafe Startup", "Singing Narration", "We Work Music", "Website Copy",
  "Theater Gig", "Screenwriters Corporation", "Charity Volley",
  "David Burke Music", "Editing Children's Book"].each do |project_name|
  if project_name == 'Cafe Startup'
    Project.create name: project_name,
                   picture: File.new('app/assets/images/web_coffee.jpg'),
                   description: "Get new customers to come to the cafe and use our services more often. We are redesigning the website and need someone with awesome copy writing skills to really beef this up. We want to potentially drive the local and young population to hang out at our shop.",
                   duration: (1..10).to_a.sample
    3.times do |i|
      ProjectSkill.create project: Project.last,
                          skill: Skill.find(i + 1)
    end
  else
    Project.create name: project_name
  end
end