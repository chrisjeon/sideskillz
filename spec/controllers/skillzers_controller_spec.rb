require 'rails_helper'

RSpec.describe SkillzersController, :type => :controller do
  before do
    @skillzer = FactoryGirl.create(:skillzer)
    @skill_seeker = FactoryGirl.create(:skill_seeker)
  end

  describe 'GET index' do
    before { get :index }

    it 'should respond with success' do
      expect(response).to be_success
    end

    it 'skillzer count should be 1' do
      expect(assigns(:skillzers).count).to eq 1
    end
  end
end
