require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  before { @skillzer = FactoryGirl.create(:skillzer) }

  describe "GET show" do
    it "returns http success" do
      get :show, id: @skillzer.id
      expect(response).to be_success
    end
  end
end