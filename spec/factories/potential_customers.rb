# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :potential_customer do
    email "MyString"
    type ""
    description "MyText"
  end
end
