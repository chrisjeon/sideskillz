# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  trait :basic_user do
    type 'basic_user'
    sequence(:email) { |n| Faker::Internet.email("foo#{n}") }
    password 'password'
    password_confirmation 'password'
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
  end

  factory :skill_seeker, traits: [:basic_user] do
    type 'SkillSeeker'
    company_name Faker::Company.name
  end

  factory :skillzer, traits: [:basic_user] do
    type 'Skillzer'
  end
end